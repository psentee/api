package main

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/go-git/go-git/v5"
)

type server struct {
	reqC chan *req

	cacheMu sync.RWMutex
	cache   map[string]map[string][]byte

	gitMu     sync.Mutex
	appRepo   *git.Repository
	lastFetch time.Time
}

type req struct {
	getReleases    chan []GLRelease
	getAppRegistry chan *appRegistry
}

func (s *server) run(ctx context.Context) {
	releases, err := getReleases(ctx)
	if err != nil {
		log.Fatalf("Initial release fetch failed: %v", err)
	}
	appRegistry, err := s.getAppRegistry(ctx)
	if err != nil {
		log.Fatalf("Initial app fetch failed: %v", err)
	}

	t := time.NewTicker(5 * 60 * time.Second)
	for {
		select {
		case r := <-s.reqC:
			if r.getReleases != nil {
				r.getReleases <- releases
			}
			if r.getAppRegistry != nil {
				r.getAppRegistry <- appRegistry
			}
		case <-t.C:
			releases, err = getReleases(ctx)
			if err != nil {
				log.Printf("Failed to fetch releases: %v", err)
			}
			appRegistry, err = s.getAppRegistry(ctx)
			if err != nil {
				log.Printf("Failed to fetch app registry: %v", err)
			}
		}
	}
}

func (s *server) getReleases(ctx context.Context) ([]GLRelease, error) {
	sresC := make(chan []GLRelease)
	select {
	case s.reqC <- &req{getReleases: sresC}:
		releases := <-sresC
		return releases, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *server) getApps(ctx context.Context) ([]*appDescriptor, error) {
	sresC := make(chan *appRegistry)
	select {
	case s.reqC <- &req{getAppRegistry: sresC}:
		res := <-sresC
		return res.apps, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *server) getAppShame(ctx context.Context) ([]*appShame, error) {
	sresC := make(chan *appRegistry)
	select {
	case s.reqC <- &req{getAppRegistry: sresC}:
		res := <-sresC
		return res.shame, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}
