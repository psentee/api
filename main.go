package main

import (
	"context"
	"flag"
	"log"
	"net/http"
)

var (
	flagBaseURL       string
	flagListen        string
	flagGitlabHost    string
	flagGitlabProject string
)

func main() {
	flag.StringVar(&flagBaseURL, "base_url", "https://flow3r.garden/api/", "Base address at which this instance runs (used for calculating proxied data URLs)")
	flag.StringVar(&flagListen, "listen", ":8080", "Address on which to listen")
	flag.StringVar(&flagGitlabHost, "gitlab_host", "git.flow3r.garden", "GitLab instance host")
	flag.StringVar(&flagGitlabProject, "gitlab_project", "flow3r/flow3r-firmware", "Name of the organization/project on GitLab")
	flag.Parse()

	ctx := context.Background()
	s := server{
		reqC:  make(chan *req),
		cache: make(map[string]map[string][]byte),
	}
	go s.run(ctx)

	http.HandleFunc("/api/apps.json", s.handleApps)
	http.HandleFunc("/api/releases.json", s.handleReleases)
	http.HandleFunc("/api/release/", s.handleReleaseMirror)
	http.HandleFunc("/api/apps/zip/", s.handleAppZip)
	http.HandleFunc("/api/apps/tar/", s.handleAppTargz)
	http.HandleFunc("/api/apps/shame.json", s.handleAppShame)
	http.HandleFunc("/api/apps/", s.handleApp)
	log.Printf("Listening on %s...", flagListen)
	http.ListenAndServe(flagListen, nil)
}
