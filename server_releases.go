package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

type GLAssetLink struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`
	TagName string `json:"tag_name"`
	URL     string `json:"url"`
}

type GLRelease struct {
	Name       string    `json:"name"`
	TagName    string    `json:"tag_name"`
	ReleasedAt time.Time `json:"released_at"`
	Assets     struct {
		Links []GLAssetLink `json:"links"`
	} `json:"assets"`
}

func getReleases(ctx context.Context) ([]GLRelease, error) {
	path := fmt.Sprintf("https://%s/api/v4/projects/%s/releases?order_by=created_at", flagGitlabHost, url.PathEscape(flagGitlabProject))
	req, err := http.NewRequestWithContext(ctx, "GET", path, nil)
	if err != nil {
		return nil, fmt.Errorf("when building request: %w", err)
	}
	res, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		return nil, fmt.Errorf("when performing request: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("invalid response code %d", res.StatusCode)
	}

	var releases []GLRelease
	j := json.NewDecoder(res.Body)
	err = j.Decode(&releases)
	if err != nil {
		return nil, fmt.Errorf("when performing request: %w", err)
	}
	return releases, nil
}

func (s *server) handleReleases(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	releases, err := s.getReleases(ctx)
	if err != nil {
		return
	}

	type partition struct {
		Name   string `json:"name"`
		URL    string `json:"url"`
		Offset string `json:"offset"`
	}

	type release struct {
		Name       string      `json:"name"`
		Tag        string      `json:"tag"`
		Partitions []partition `json:"partitions"`
	}

	var resp []release
	for _, rel := range releases {
		var partitions []partition
		offsets := map[string]int64{
			"bootloader":      0,
			"partition-table": 0x8000,
			"recovery":        0x10000,
			"flow3r":          0x90000,
		}
		for _, pname := range []string{"bootloader", "partition-table", "recovery", "flow3r"} {
			partitions = append(partitions, partition{
				Name:   pname,
				URL:    fmt.Sprintf("%srelease/%s/%s.bin", flagBaseURL, rel.TagName, pname),
				Offset: fmt.Sprintf("0x%x", offsets[pname]),
			})
		}
		resp = append(resp, release{
			Name:       rel.Name,
			Tag:        rel.TagName,
			Partitions: partitions,
		})
	}
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	j := json.NewEncoder(w)
	j.Encode(resp)
}
